// ==UserScript==
// @name        Show Alliance Member Race
// @namespace   http://userscripts.xcom-alliance.info/
// @description Shows the race for the members of your alliance when viewing the Members tab of the Diplomacy page
// @version     1.0
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/alliance_members.php
// @updateURL 	http://userscripts.xcom-alliance.info/alliance_member_race/show_alliance_member_race.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/alliance_member_race/show_alliance_member_race.user.js
// @icon 		http://userscripts.xcom-alliance.info/alliance_member_race/icon.png
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_xmlhttpRequest
// ==/UserScript==
