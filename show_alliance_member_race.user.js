// ==UserScript==
// @name        Show Alliance Member Race
// @namespace   http://userscripts.xcom-alliance.info/
// @description Shows the race for the members of your alliance when viewing the Members tab of the Diplomacy page
// @version     1.0
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/alliance_members.php
// @updateURL 	http://userscripts.xcom-alliance.info/alliance_member_race/show_alliance_member_race.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/alliance_member_race/show_alliance_member_race.user.js
// @icon 		http://userscripts.xcom-alliance.info/alliance_member_race/icon.png
// @grant       GM_getValue
// @grant       GM_setValue
// @grant       GM_xmlhttpRequest
// ==/UserScript==


/*************************************************************************************
	Shared methods
*************************************************************************************/
function getUniverse() {
	return window.location.host.substr(0, window.location.host.indexOf('.'));
};


/*************************************************************************************
	Use AJAX to visit the profile for a pilot and scrape their race
*************************************************************************************/
function fetchPilotRace(pilotId) {
	var url = 'profile.php?id=' + encodeURIComponent(pilotId);
	GM_xmlhttpRequest({
		method: "GET",
		url: url,
		onload: function(response) {
			var race_match = response.responseText.match(/std\/races\/([^_]+_64x64f*)\.jpg/i);
			if (race_match) {
				GM_setValue(getUniverse()+pilotId, race_match[1]);
				setPilotRaceImg(pilotId, race_match[1]);
			}
		}
	});
};


/*************************************************************************************
	Set the race image for a pilot
*************************************************************************************/
function setPilotRaceImg(pilot_id, pilot_race) {
	var raceImg = document.getElementById('RaceImg'+pilot_id);
	if (raceImg) raceImg.src = raceImg.src.replace('none_64x64.png', pilot_race + '.jpg');
};


/*************************************************************************************
	The magic starts here...
*************************************************************************************/
var pilotEls = document.querySelectorAll('img[title="View Profile"]');
var img_path = pilotEls[0].src.substring(0, pilotEls[0].src.indexOf('/ships/')+1 );
for (var i=0;i<pilotEls.length;i++) {
	var pilot_id = String(pilotEls[i].getAttribute('onclick')).replace(/[^\d]*/g,'');
	pilotEls[i].parentNode.innerHTML += '<img id="RaceImg' + pilot_id + '" src="' + img_path + 'races/none_64x64.png" class="raceimage" style="position:absolute;left:130px;width:32px;margin-top:-50px;"/>';
	var pilot_race = GM_getValue(getUniverse()+pilot_id, '');
	if (pilot_race == '') {
		fetchPilotRace(pilot_id);
	} else {
		setPilotRaceImg(pilot_id, pilot_race);
	}
}
